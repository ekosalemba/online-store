# Online Store

Manage stock item with handle concurrent request

**Case**

Dalam setiap transaksi, harus mengurangi jumlah stok produk sesuai dengan jumlah item yang dibeli. Namun jika stok kurang dari jumlah yang akan dipesan, maka pesanan tidak bisa dilanjutkan. Tetapi karena terdapat pemesanan dalam jumlah yang banyak terhadap 1 jenis produk dalam waktu yang bersamaan (_concurrent_), maka sistem akan memproses order tersebut secara bersamaan dalam hal pengecekak/pengurangan stok produk. Hal tersebut menjadi tidak _relevan_ mengingat kita harus melihat stok produk secara bergantian (_sequential_) untuk order yang mempunyai jenis produk yang sama (product_id sama).

Contoh terdapat produk dengan id 1 mempunyai stok produk sejumlah 10, kemudian 2 user melakukan order dalam waktu yang sama untuk produk tersebut. User1 order sejumlah 3 item, user2 order sejumlah 8 item.

| User 1 | User 2 |
| ------ | ------ |
| select stock from product where id 1 => **10** (cukup, 10 >=3) |  |
|  | select stock from product where id 1 => **10** (cukup, 10 >=8) |
| update product set stock = stock - 3 where id = 1 => stok menjadi **7** |  |
|  | update product set stock = stock - 8 where id = 1 => stok menjadi **-1** |

Dengan alur seperti diatas, kedua transksi sukses dilakukan dan stok produk yang ada di database sekarang adalah **-1**. Namun jika kita mengkalkulasi data yang sebenarnya seharusnya User2 tidak dapat melanjutkan transaksi karena jumlah item yang dipesan melebihi stok yang ada.

- Stok awal = 10
- User1 order 3, stok = 7
- User2 order 8, gagal => stok tidak mencukupi

**Solusi**

Salah satu solusi yang paling mudah untuk di implementasi dari problem diatas adalah menggunakan row locking transaction. Yaitu dengan melakukan lock terhadap row(stok produk) yang akan diupdate sampai update stok selesai. Sehingga transaksi selanjutnya dengan id produk yang sama akan menunggu sampai proses update stok transaksi sebelumnya. Berikut ilustrasi row locking transaction untuk kasus sebelumnya.


| User 1 | User 2 |
| ------ | ------ |
|BEGIN||
| select stock from product where id 1 FOR UPDATE => **10** (cukup, 10>=3) | BEGIN |
| update product set stock = stock - 3 where id = 1 => stok menjadi **7** | BLOCK row id = 1 |
|COMMIT||
||select stock from product where id 1 FOR UPDATE => **7** (tidak cukup, 7 < 8)|
||COMMIT|

Dengan demikian data stok produk lebih terjaga ke-valid an nya karena ada lock row saat akan ada perubahan data.

**Catatan**
- Projek ini dibuat dengan bahasa PHP dengan framework Laravel 6.2x
- Database menggunakan MySql, belum menggunakan migrations tetapi dump database berada di dalam repository
- Root path URL (/) berisi Swagger UI
- Terdapat 1 Feature test (/tests/Feature/OrderTest.php) untuk menjalankan fungsi checckout (feature test skip migrations)