/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.18 : Database - online_store_v1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_price` double DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `order` */

insert  into `order`(`order_id`,`order_price`) values (1,17500),(2,20000);

/*Table structure for table `order_item` */

DROP TABLE IF EXISTS `order_item`;

CREATE TABLE `order_item` (
  `oi_id` int(11) NOT NULL AUTO_INCREMENT,
  `oi_order_id` int(11) DEFAULT NULL,
  `oi_product_id` int(11) DEFAULT NULL,
  `oi_qty` int(11) DEFAULT NULL,
  `oi_price` double DEFAULT NULL,
  `oi_total_price` double DEFAULT NULL,
  PRIMARY KEY (`oi_id`),
  KEY `oi_order_id` (`oi_order_id`),
  KEY `oi_product_id` (`oi_product_id`),
  CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`oi_order_id`) REFERENCES `order` (`order_id`),
  CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`oi_product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `order_item` */

insert  into `order_item`(`oi_id`,`oi_order_id`,`oi_product_id`,`oi_qty`,`oi_price`,`oi_total_price`) values (1,1,1,3,5000,15000),(2,1,2,1,2500,2500),(3,2,1,3,5000,15000),(4,2,2,2,2500,5000);

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `product_stock` int(11) DEFAULT '0',
  `product_price` double DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `product` */

insert  into `product`(`product_id`,`product_name`,`product_stock`,`product_price`) values (1,'Book',9,5000),(2,'Pen',32,2500),(3,'Pencil',50,1500);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
