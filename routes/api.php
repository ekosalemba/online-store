<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// products
Route::get('v1/products', 'ProductController@getProducts');

//orders
Route::get('v1/orders', 'OrderController@getOrders');
Route::post('v1/checkout', 'OrderController@checkout');