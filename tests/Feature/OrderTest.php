<?php

namespace Tests\Feature;

use Tests\TestCase;

class OrderTest extends TestCase
{

    public function testBasicTest()
    {
        $body = [];
        $body[] = ['productId' => 1, 'productQty' => 3];
        $body[] = ['productId' => 2, 'productQty' => 2];
        $response = $this->json(
            'POST', //Method
            '/api/v1/checkout', //Route
            $body, //JSON Body request
            ['Content-Type' => 'application/json'] // headers
        );
        $this->assertEquals(200, $response->status());
        $responseData = $response->decodeResponseJson()['response'];
        $this->assertEquals(true, $responseData['success']);
    }
}
