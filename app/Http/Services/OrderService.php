<?php


namespace App\Http\Services;


use App\Http\Domains\BaseResponse;
use App\Http\Repositories\OrderRepository;

class OrderService
{
    public $orderRepo;
    public $response;

    public function __construct()
    {
        $this->orderRepo = new OrderRepository();
        $this->response = new BaseResponse();
    }

    public function getOrders()
    {
        $orders = $this->orderRepo->getOrders();
        $productResponse = $this->buildGetOrdersResponse($orders);
        $this->response->success = true;
        $this->response->message = "OK";
        $this->response->data = $productResponse;
        return $this->response;
    }

    private function buildGetOrdersResponse($orders)
    {
        $response = [];
        $lastOrderId = 0;
        for ($i = 0; $i < sizeof($orders); $i++) {
            if ($lastOrderId == $orders[$i]->order_id) {
                continue;
            }
            $products = [];
            for ($iProduct = 0; $iProduct < sizeof($orders); $iProduct++) {
                if ($orders[$iProduct]->order_id == $orders[$i]->order_id) {
                    $products[] = [
                        "id" => $orders[$iProduct]->oi_product_id,
                        "name" => $orders[$iProduct]->product_name,
                        "price" => $orders[$iProduct]->oi_price,
                        "qty" => $orders[$iProduct]->oi_qty,
                        "totalPrice" => $orders[$iProduct]->oi_total_price
                    ];
                }
            }
            $item = [
                "id" => $orders[$i]->order_id,
                "price" => $orders[$i]->order_price,
                "products" => $products,
            ];
            $response[] = $item;
            $lastOrderId = $orders[$i]->order_id;
        }
        return $response;
    }

    public function checkout($request)
    {
        $response = $this->orderRepo->checkout($request);
        $this->response->success = $response->success;
        $this->response->data = $response->data;
        return $this->response;
    }
}