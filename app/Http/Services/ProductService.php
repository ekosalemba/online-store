<?php


namespace App\Http\Services;


use App\Http\Domains\BaseResponse;
use App\Http\Repositories\ProductRepository;

class ProductService
{
    public $productRepo;
    public $response;

    public function __construct()
    {
        $this->productRepo = new ProductRepository();
        $this->response = new BaseResponse();
    }

    public function getProducts()
    {
        $products = $this->productRepo->getAllProducts();
        $this->response->success = true;
        $this->response->message = "OK";
        $this->response->data = $products;
        return $this->response;
    }
}