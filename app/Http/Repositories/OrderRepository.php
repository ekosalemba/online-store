<?php


namespace App\Http\Repositories;


use App\Http\Domains\BaseResponse;
use App\Http\Models\Order;
use App\Http\Models\OrderItem;
use App\Http\Models\Product;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    public function __construct()
    {
    }

    public function getOrders()
    {
        return Order::from('order as o')
            ->select('o.order_id', 'o.order_price', 'oi.oi_product_id', 'p.product_name', 'oi.oi_qty', 'oi.oi_price', 'oi.oi_total_price')
            ->join('order_item as oi', 'o.order_id', '=', 'oi.oi_order_id')
            ->leftJoin('product as p', 'oi.oi_product_id', '=', 'p.product_id')
            ->get();
    }

    public function checkout($request = [])
    {
        $result = new BaseResponse();
        $result->success = false;
        try {
            DB::beginTransaction();
            $productIds = [];
            for ($i = 0; $i < count($request); $i++) {
                $productIds[] = $request[$i]['productId'];
            }
            $products = Product::lockForUpdate()->whereIn('product_id', $productIds)->get();
            if (sizeof($products) != sizeof($request)) {
                DB::rollBack();
                return $result;
            }

            // check availability stock
            for ($i = 0; $i < count($request); $i++) {
                if ($request[$i]['productQty'] > $products[$i]->product_stock) {
                    DB::rollBack();
                    $result->message = "Insufficient stock";
                    return $result;
                }
            }

            // sum total price
            $totalPrice = 0;
            for ($i = 0; $i < count($products); $i++) {
                $totalPrice += $products[$i]->product_price * $request[$i]['productQty'];
            }

            // insert order
            $order = new Order();
            $order->order_price = $totalPrice;
            $order->save();

            // insert order item and reduce stock
            for ($i = 0; $i < count($products); $i++) {
                $qty = $request[$i]['productQty'];
                $price = $products[$i]->product_price;
                $orderItem = new OrderItem();
                $orderItem->oi_order_id = $order->order_id;
                $orderItem->oi_product_id = $products[$i]->product_id;
                $orderItem->oi_qty = $qty;
                $orderItem->oi_price = $price;
                $orderItem->oi_total_price = $qty * $price;
                $orderItem->save();

                //reduce stock
                DB::update("update product set product_stock = product_stock-? where product_id = ?", [$qty, $products[$i]->product_id]);
            }
            $result->success = true;
            $result->message = "OK";
            $result->data = [
                'orderId' => $order->order_id,
                'totalPrice' => $order->order_price,
            ];
            DB::commit();
            return $result;
        } catch (\Exception $e) {
            DB::rollback();
            \Log::error($e);
            $result->message = "Failed to create order";
            return $result;
        }
    }
}