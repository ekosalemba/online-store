<?php


namespace App\Http\Repositories;


use App\Http\Models\Product;

class ProductRepository
{
    public function __construct()
    {
    }

    public function getAllProducts()
    {
        return Product::from('product as p')
            ->select('p.product_id as id', 'p.product_name as name', 'p.product_stock as stock', 'p.product_price as price')
            ->get();
    }
}