<?php

namespace App\Http\Domains;


class BaseResponse
{
    public $success;
    public $message;
    public $data;
}