<?php

namespace App\Http\Middleware;

use Closure;

class ApiDataLogger
{
    private $startTime;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->startTime = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if (env('API_DATALOGGER', true)) {
            $endTime = microtime(true);
            $filename = 'api_datalogger_' . date('Y-m-d') . '.log';
            $logMaxFile = (int)env('LOG_MAX_FILES', 100);
            $dataToLogJson = [];
            $dataToLogJson['ipAddress'] = $request->ip();
            $dataToLogJson['url'] = $request->fullUrl();
            $dataToLogJson['method'] = $request->method();
            $dataToLogJson['request'] = json_decode($request->getContent());
            $dataToLogJson['reqTimestamp'] = date('Y-m-d H:i:s', $this->startTime);
            $dataToLogJson['resTimestamp'] = date("Y-m-d h:i:s");
            $dataToLogJson['duration'] = number_format($endTime - $this->startTime, 3);
            $dataToLogJson['response'] = json_decode($response->getContent());
            $dataToLogJson['status'] = $response->status();
            $dataToLogJson = json_encode($dataToLogJson, JSON_UNESCAPED_SLASHES);
            if (!is_dir(storage_path('logs/api'))) {
                mkdir(storage_path('logs/api'));
            }
            \File::append(storage_path('logs/api' . DIRECTORY_SEPARATOR . $filename), $dataToLogJson . str_repeat("\n", 1));
            $getAllDirs = scandir(storage_path('logs/api'), 1);
            $dataLog = [];
            foreach ($getAllDirs as $index => $file) {
                if (!in_array($file, ['.', '..'])) {
                    $dataLog[] = $file;
                }
            }
            if (count($dataLog) > $logMaxFile) {
                foreach ($dataLog as $index => $file) {
                    if ($index >= $logMaxFile) {
                        unlink(storage_path('logs/api' . DIRECTORY_SEPARATOR . $file));
                    }
                }
            }
        }
    }
}