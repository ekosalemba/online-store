<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class FormatResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $original = $response->getContent();
        $newResponse = [
            "response" => json_decode($original),
            "responseInfo" => [
                "status" => $response->status(),
                "source" => "ORDER_SERVICE",
                "logId" => uniqid(),
            ]
        ];
        $response->setContent(json_encode($newResponse));
        return $response;
    }
}
