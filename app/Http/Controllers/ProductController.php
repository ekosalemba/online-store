<?php


namespace App\Http\Controllers;


use App\Http\Services\ProductService;
use Illuminate\Routing\Controller as Controller;

class ProductController extends Controller
{
    public $productService;

    public function __construct()
    {
        $this->middleware('apiDataLogger');
        $this->productService = new ProductService();
    }

    public function getProducts()
    {
        $response = $this->productService->getProducts();
        return response()->json($response, 200);
    }
}