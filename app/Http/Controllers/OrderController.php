<?php


namespace App\Http\Controllers;


use App\Http\Services\OrderService;
use Illuminate\Routing\Controller as Controller;

class OrderController extends Controller
{
    public $orderService;

    public function __construct()
    {
        $this->middleware('apiDataLogger');
        $this->orderService = new OrderService();
    }

    public function getOrders()
    {
        $response = $this->orderService->getOrders();
        return response()->json($response, 200);
    }

    public function checkout()
    {
        $response = $this->orderService->checkout(json_decode(request()->getContent(), true));
        return response()->json($response, 200);
    }
}