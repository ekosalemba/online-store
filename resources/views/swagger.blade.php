<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Swagger UI</title>
    <link rel="stylesheet" type="text/css" href="/assets/swagger-ui/css/swagger-ui.css">
    <link rel="icon" type="image/png" href="/assets/swagger-ui/images/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/assets/swagger-ui/images/favicon-16x16.png" sizes="16x16"/>
    <style>
        html {
            box-sizing: border-box;
            overflow: -moz-scrollbars-vertical;
            overflow-y: scroll;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        body {
            margin: 0;
            background: #fafafa;
        }
    </style>
</head>

<body>
<div id="swagger-ui"></div>

<script src="/assets/swagger-ui/js/swagger-ui-bundle.js"></script>
<script src="/assets/swagger-ui/js/swagger-ui-standalone-preset.js"></script>
<script>
    window.onload = function () {
        var path = "";
        var configObject = JSON.parse('{"urls":[{"url":"'+path+'/api-spec","name":"Online Store REST Web Service V 1.0"}],"deepLinking":false,"displayOperationId":false,"defaultModelsExpandDepth":1,"defaultModelExpandDepth":1,"defaultModelRendering":"example","displayRequestDuration":false,"docExpansion":"list","showExtensions":false,"showCommonExtensions":false,"supportedSubmitMethods":["get","put","post","delete","options","head","patch","trace"],"validatorUrl":null}');
        var oauthConfigObject = JSON.parse('{"clientId":"clientId","clientSecret":"clientSecret","scopeSeperator":" ","useBasicAuthenticationWithAccessCodeGrant":false}');

        // Apply mandatory parameters
        configObject.dom_id = "#swagger-ui";
        configObject.presets = [SwaggerUIBundle.presets.apis, SwaggerUIStandalonePreset];
        configObject.layout = "StandaloneLayout";

        // If oauth2RedirectUrl isn't specified, use the built-in default
        if (!configObject.hasOwnProperty("oauth2RedirectUrl"))
            configObject.oauth2RedirectUrl = window.location.href.replace("swagger/index", "oauth2-redirect.html");

        // Build a system
        const ui = SwaggerUIBundle(configObject);

        // Apply OAuth config
        ui.initOAuth(oauthConfigObject);
    }
</script>
</body>
</html>
